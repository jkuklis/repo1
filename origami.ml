type point = float * float;;
(* point reprezentuje punkt lub wektor zaczepiony w (0,0) *)

type kartka = point -> int;;

let round ((x,y) : point) =
	let x = (if abs_float (x -. (floor (x+.0.1))) < 0.00000001 then floor (x+.0.1) else x)
	and y = (if abs_float (y -. (floor (y+.0.1))) < 0.00000001 then floor (y+.0.1) else y)
	in ((x,y) : point);;

let scale k ((x,y) : point) = ((k *. x, k *. y) : point);;
(* przeskalowanie o skali k wektora a) *)

let add ((ax,ay) : point) ((bx,by) : point) = ((ax +. bx, ay +. by) : point);;
(* a,b - wektory, add a b = a + b *)

let sub (a : point) (b : point) = add (scale (-1.) a) b;;
(* a,b - wektory, sub a b = b - a *)

let pythsq ((x,y) : point) = x *. x +. y *. y;;
(* kwadrat odleglosci a od (0,0) *)

let distsq (a : point) (b : point) = pythsq (sub a b);;
(* kwadrat odleglosci miedzy a,b *)

let dot_prod (a : point) (b : point) (c : point) =
	let (vx,vy) = sub a b and (ux,uy) = sub a c in 
		vx *. ux +. vy *. uy;;
(* iloczyn skalarny wektorow ab i ac *)

let cross_prod (a : point) (b : point) (c : point) =
	let (vx,vy) = sub a b and (ux,uy) = sub a c in
		vx *. uy -. ux *. vy;;
(* wartosc iloczynu wektorowego ab i ac *)

let proj (a : point) (b : point) (c : point) =
	let cp = cross_prod a b c and lsq = distsq b a in
		let hsq = cp *. cp /. lsq and d = (if dot_prod a b c < 0. then -1. else 1.) in
			let sc = d *. sqrt (((distsq c a) -. hsq) /. lsq) in
				round (add (scale sc b) (scale (1.-.sc) a));;
(* rzut c na ab *)
(* lsq - kwadrat dlugosci ab *)
(* hsq - kwadrat odleglosci c od ab *)
(* proj - rzut c na ab, obliczany jako a + sc * ab *)
(* sc - dlugosc (a,proj) przez dlugosc (ab) *)
(* dlugosc (a,proj) oblczana przy pomocy Pitagorasa dla (a,proj,c) *)

let reflect (a : point) (b : point) (c : point) =	sub c (scale 2. (proj a b c));;
(* odbicie punktu c wzgledem prostej ab *)

let prostokat ((x1,y1) : point) ((x2,y2) : point) = 
	(function ((x,y) : point) -> 
		if (x1 <= x && x <= x2 && y1 <= y && y <= y2) then 1 else 0 : kartka);;

let kolko (s : point) r =
	(function (p : point) ->	
		if distsq s p <= r *. r then 1 else 0 : kartka);;

let zloz (p1 : point) (p2 : point) (k : kartka) =
	(function (p : point) ->
		let cp = cross_prod p1 p2 p in
			if (cp < 0.) then 0
			else if (cp = 0.) then k p
			else (k p) + (k (reflect p1 p2 p)) : kartka);;

let rec skladaj (li : (point * point) list) (k : kartka) =
	let f kar (p1,p2) = zloz p1 p2 kar in
		List.fold_left f k li;;
