#use "pMap.ml"
		
exception Cykliczne

type 'a info = {
	mutable vis : int ;
	mutable nei : 'a list } 

let add_one a ali m =
	try 
		let f = find a m in
			(if f.nei = [] then f.nei <- ali; m)
	with Not_found -> add a { vis = -1; nei = ali } m
	
let rec add_list ali m =
	 match ali with
	| [] -> m
	| h::t -> add_list t (add_one h [] m)
										
let make_dict li =
	let rec pom l m =
		match l with
		| [] -> m
		| (a,ali)::t ->
			let m = add_one a ali m in
			let m = add_list ali m in
			pom t m
	in pom li empty
												
let topol li =
	let dict = make_dict li in
	let rec dfs k d acc = 
		if (d.vis = 1) then acc else
		if (d.vis = 0) then raise Cykliczne else
		( d.vis <- 0;
		let acc = List.fold_left (fun a h -> dfs h (find h dict) a) acc d.nei
		in (d.vis <- 1; k::acc) )
	in foldi dfs dict []