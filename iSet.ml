(* chyba dziala *)
(* pozyczylem sobie bezpieczne dodawania ^^ *)

let add_2 a b =
	if (b < 0) then a + b 
	else if (a >= max_int - b) then max_int
	else a + b
	
let add_3 a b c =
	add_2 (add_2 a b) c

(* komparator, ustalony na sztywno *)
let cmp a (p,k) =
	if (p <= a && a <= k) then 0
	else if (a < p) then -1
	else 1
		
(* left, interval, right, height, size *)		
type set =
	| Empty
	| Node of set * (int * int) * set * int * int

let height = function
	| Empty -> 0
	| Node (_, _, _, h, _) -> h

let size = function
	| Empty -> 0
	| Node (_, _, _, _, n) -> n

let len (p,k) = add_3 k (-p) 1

(* obliczenie size *)
let norm = function
	| Empty -> Empty
	| Node (l, v, r, h, _) -> Node (l, v, r, h, add_3 (size l) (size r) (len v))

let make l k r = norm (Node (l, k, r, max (height l) (height r) + 1, 0))

let bal l k r =
	let hl = height l
	and hr = height r in
	if hl > hr + 2 then
		match l with
		| Node (ll, lk, lr, _, _) ->
			if height ll >= height lr then make ll lk (make lr k r)
			else 
			(match lr with
			| Node (lrl, lrk, lrr, _, _) ->
			make (make ll lk lrl) lrk (make lrr k r)
			| Empty -> assert false)
		| Empty -> assert false
	else if hr > hl + 2 then
		match r with
		| Node (rl, rk, rr, _, _) ->
			if height rr >= height rl then make (make l k rl) rk rr
			else
			(match rl with
			| Node (rll, rlk, rlr, _, _) ->
			make (make l k rll) rlk (make rlr rk rr)
			| Empty -> assert false)
		| Empty -> assert false
	else norm (Node (l, k, r, max hl hr + 1, 0))
	
let rec min_elt = function
	| Node (Empty, k, _, _, _) -> k
	| Node (l, _, _, _, _) -> min_elt l
	| Empty -> raise Not_found

let rec remove_min_elt = function
	| Node (Empty, _, r, _, _) -> r
	| Node (l, k, r, _, _) -> bal (remove_min_elt l) k r
	| Empty -> invalid_arg "PSet.remove_min_elt"

let merge t1 t2 =
	match t1, t2 with
	| Empty, _ -> t2
	| _, Empty -> t1
	| _ ->
		let k = min_elt t2 in
		bal t1 k (remove_min_elt t2)
	
let empty = Empty

let is_empty x = (x = Empty)

(* dodajemy tylko takie, ktore nie maja czesci wspolnej z istniejacymi *)
(* zmiana: cmp x k -> cmp b k, w zwiazku z powyzszym wystarczajace kryterium *)
let rec add_one ((b,e) as x) = function
	| Node (l, k, r, h, n) ->
		let c = cmp b k in
		if c = 0 then Node (l, x, r, h, n)
		else if c < 0 then
			let nl = add_one x l in
			bal nl k r
		else
			let nr = add_one x r in
			bal l k nr
	| Empty -> Node (Empty, x, Empty, 1, len x)

(* (p,k) przeciete z t = 0 *)
let safe_add (p,k) t =
	if (p > k) then t
	else add_one (p,k) t		

(* usuniete cmp *)
(* laczymy tylko sety l < v < r *)
let rec join l v r =
	match (l, r) with
	|	(Empty, _) -> add_one v r
	| (_, Empty) -> add_one v l
	| (Node(ll, lv, lr, lh, _), Node(rl, rv, rr, rh, _)) ->
		if lh > rh + 2 then bal ll lv (join lr v r) else
		if rh > lh + 2 then bal (join l v rl) rv rr else
		make l v r

(* z oryginalnego pliku *)
(* zamiast boola zwraca przedzial, w ktorym jest x, albo (x+1, x-1) *)
let rec org_split x set =
	let rec loop x = function
	| Empty ->
 		(Empty, (x+1, x-1) , Empty)
	| Node (l, v, r, _, _) ->
		let c = cmp x v in
		if c = 0 then (l, v, r)
		else if c < 0 then
			let (ll, pres, rl) = loop x l in (ll, pres, join rl v r)
		else
			let (lr, pres, rr) = loop x r in (join l v lr, pres, rr)
	in loop x set

let rec split x set =
	let (l, (p,k), r) = org_split x set in 
	if (p > k) then (l, false, r)
	else (safe_add (p,x-1) l, true, safe_add (x+1,k) r)

(* patrzymy, czy (p,k) "laczy sie" z jakims innym przedzialem *)
let add (p,k) t =
	let (l, (pb,pe), _) = org_split (p-1) t
	and (_, (kb,ke), r) = org_split (k+1) t in
		let mid_el = 
			(if (pb > pe && kb > ke) then (p,k)
			else if (pb > pe) then (p,ke)
			else if (kb > ke) then (pb,k)
			else (pb,ke))
		in join l mid_el r

let remove (p,k) t =
	let (l, _, _) = split p t
	and (_, _, r) = split k t in
		if r = Empty then l
		else join l (min_elt r) (remove_min_elt r)
		
let mem x t =
	let (_, b, _) = split x t in b
	
let exists = mem

let iter f set =
	let rec loop = function
		| Empty -> ()
		| Node (l, k, r, _, _) -> loop l; f k; loop r 
	in loop set

let fold f set acc =
	let rec loop acc = function
		| Empty -> acc
		| Node (l, k, r, _, _) ->
			loop (f k (loop acc l)) r
	in loop acc set

let below n set = 
	let (l, b, r) = split n set in
		if (size l > max_int - 2) then max_int 
		else (size l) + (if b then 1 else 0)
			
let elements set = 
	let rec loop acc = function
		| Empty -> acc
		| Node(l, k, r, _, _) -> loop (k :: loop acc r) l
	in loop [] set