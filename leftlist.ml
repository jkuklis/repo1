type 'a queue = 
	| Leftlist of 'a queue * 'a * int * 'a queue
	| Null;;
(* schemat: lewe poddrzewo, priorytet - wartosc wezla, najprawsza wysokosc, prawe poddrzewo *)

let empty = Null;;

let is_empty q = (q = Null);;

exception Empty;;

let rec join q1 q2 =
	match (q1,q2) with
	| (Null,_) -> q2
	| (_,Null) -> q1
	| (Leftlist(l1,a1,h1,r1),Leftlist(l2,a2,h2,r2)) ->
		if (a1 <= a2) then
			let rq = join r1 q2
				in match (l1,rq) with
				| (Null,_) -> Leftlist(rq,a1,1,Null)
				| (_,Null) -> Leftlist(l1,a1,1,Null)
				| (Leftlist(_,_,hl,_),Leftlist(_,_,hr,_)) ->
					if (hl >= hr) then Leftlist(l1,a1,hr+1,rq) else Leftlist(rq,a1,hl+1,l1)
		else
			join q2 q1;;
(* sklejanie kolejek - dla niepustych wybieramy q o wiekszym priorytecie *)
(* nastepnie kleimy prawe poddrzewo q z calym drugim drzewem, *)
(* sprawdzamy, czy nowe drzewo ma wieksza najprawsza glebokosc od lewego poddrzewa q, *)
(* w zaleznosci od tego odpowiednio ustawiamy je jako poddrzewa wychodzace z korzenia q *)

let add e q = join q (Leftlist(Null,e,1,Null));;
(* dodawanie elementu poprzez sklejanie drzew *)

let delete_min q =
	match q with
	| Null -> raise(Empty)
	| Leftlist(q1,a,_,q2) -> (a,join q1 q2);;

let q = add 5 q;;
let p = add 4 p;;
let pq = join p q;;