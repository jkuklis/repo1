let mini x y = if (x < y) then x else y;;
let maxi x y = if (x > y) then x else y;;
let abs x = if (x >= 0.) then x else (-.x);;

let minimum l =
	match l with
	| (a, b, c, d) -> mini (mini a b) (mini c d);;
(* minimum z czterech elementow *)

let maksimum l =
	match l with
	| (a, b, c, d) -> maxi (maxi a b) (maxi c d);;
(*maksimum z czterech elementow *)

type wartosc =
	| InterNan
	| Inter1 of float * float
	| Inter2 of float * float * float * float;;
(* InterNan to nan *)
(* Inter1(p,k) to przedzial [p,k] *)
(* Inter2(p1,k1,p2,k2) to suma [p1,k1] i [p2,k2], k1 <= k2 *)
(* wlasnosc $: jezeli Inter2, to p1 = -inf, k2 = inf *)

let inf = infinity;;
let neg_inf = neg_infinity;;
let all = Inter1(neg_inf, inf);;

let wartosc_dokladnosc x p =
	let dif = abs(p *. x /. 100.) 
		in Inter1 (x -. dif , x +. dif);;

let wartosc_od_do x y = Inter1(x,y);;

let wartosc_dokladna x = wartosc_od_do x x;;

let in_wartosc x y =
	match x with
	| InterNan -> false
	| Inter1(p,k) -> (p <= y && y <= k)		
	| Inter2(_,k1,p2,_) -> not (k1 < y && y < p2);;
(* dla Inter2: wl. $ *)

let min_wartosc x = 
	match x with
	| InterNan -> nan
	| Inter1(p,_) -> p
	| Inter2(p1,_,_,_) -> p1;;

let max_wartosc x =
	match x with
	| InterNan -> nan
	| Inter1(_,k) -> k
	| Inter2(_,_,_,k2) -> k2;;

let sr_wartosc x = 
	match x with
	| InterNan -> nan
	| Inter1(p,k) -> if (p = neg_inf && k = inf) then nan else (p +. k) /. 2.
	| Inter2(_,_,_,_) -> nan;;
(* dla Inter2: wl $ *) 

let merge x =
	match x with
	| InterNan -> InterNan
	| Inter1(_,_) -> x
	| Inter2(p1,k1,p2,k2) -> if (k1 < p2) then x else Inter1((mini p1 p2), (maxi k1 k2));;
(* Inter2 -> Inter1 jezeli sie da, warunek wystarczajacy, bo wl. $ *)

let rec merge2 x y =
	match (x,y) with
	| (InterNan,_) -> InterNan
	| (_,InterNan) -> InterNan
	| (Inter1(xp,xk),Inter1(yp,yk)) -> 
		if (xk >= yk) then 
			merge (Inter2(yp,yk,xp,xk))
		else 	
			merge (Inter2(xp,xk,yp,yk))
	| (Inter1(xp,xk),Inter2(yp1,yk1,yp2,yk2)) ->
		if (yk1 = 0.) then merge2 (merge2 x (Inter1(yp1,yk1))) (Inter1(yp2,yk2))
		else merge2 (merge2 x (Inter1(yp2,yk2))) (Inter1(yp1,yk1))
	| (Inter2(_,_,_,_),Inter1(_,_)) -> merge2 y x
	| (Inter2(xp1,xk1,xp2,xk2),Inter2(yp1,yk1,yp2,yk2)) ->
		let a = merge2 ( Inter1(xp1,xk1) ) ( Inter1(yp1,yk1) )
		and b = merge2 ( Inter1(xp2,xk2) ) ( Inter1(yp2,yk2) )
			in merge2 a b;;
(* zakladamy, ze argumenty merge2 sa wynikami odpowiednich operacji dla mnozenia i dzielenia *)
(* to znaczy, ze da sie je sprowadzic do zbioru zawierajacego jeden lub dwa rozlaczne przedzialy *)
(* przypadek Inter1, Inter2 moze pojawic sie w np. w kontekscie dzielenia [1] / [(-inf,1)u(2,inf)] *)
(* tzn. wtedy, gdy dzielimy przez zbior dwoch przedzialow, przy czym jeden z nich zawiera 0 *)
(* dla takich przypadkow dwa sposrod trzech otrzymanych przedzialow zawieraja 0 *)

let przeciwny x =
	match x with
	| InterNan -> InterNan
	| Inter1(p,k) -> Inter1(-.k,-.p)
	| Inter2(p1,k1,p2,k2) -> Inter2(-.k2,-.p2,-.k1,-.p1);;

let rec plus a b =
	match (a,b) with
	| (InterNan,_) -> InterNan
	| (_,InterNan) -> InterNan
	| (Inter1(ap,ak),Inter1(bp,bk)) -> Inter1(ap +. bp, ak +. bk)
	| (Inter2(ap1,ak1,ap2,ak2),Inter1(bp,bk)) -> merge (Inter2(ap1, ak1+.bk, ap2+.bp, ak2))
	| (Inter1(_,_), Inter2(_,_,_,_)) -> plus b a
	| (Inter2(_,_,_,_), Inter2(_,_,_,_)) -> all;;
(* dla Inter2 i Inter1, z wl $: ap1 = -inf i ak2 = inf *)
(* dla dwoch Inter2, z wl $: mozemy wziac dowolnie duza liczbe ujemna *)
(* i dowolnie duza liczbe dodatnia, stad ich suma moze byc kazda liczba *)

let minus a b = plus a (przeciwny b);;

let rec razy a b =
	match (a,b) with
	| (InterNan,_) -> InterNan
	| (_,InterNan) -> InterNan
	| (Inter1(ap,ak),Inter1(bp,bk)) ->
		let l1 = if (ap = 0. || bp = 0.) then 0. else ap *. bp 
		and l2 = if (ap = 0. || bk = 0.) then 0. else ap *. bk
		and l3 = if (ak = 0. || bp = 0.) then 0. else ak *. bp
		and l4 = if (ak = 0. || bk = 0.) then 0. else ak *. bk
			in let l = (l1, l2, l3, l4) 
				in let bot = minimum l and top = maksimum l 
					in Inter1(bot,top)
	| (Inter2(ap1,ak1,ap2,ak2),Inter1(bp,bk)) ->
		let x = razy (Inter1(ap1,ak1)) b
		and y = razy (Inter1(ap2,ak2)) b
			in merge2 x y
	| (Inter1(_,_), Inter2(_,_,_,_)) -> razy b a
	| (Inter2(_,_,_,_), Inter2(bp1,bk1,bp2,bk2)) -> 
		let r1 = razy a (Inter1(bp1,bk1)) 
		and r2 = razy a (Inter1(bp2,bk2))
			in merge2 r1 r2;;
(* dzielimy na przedzialy Inter1 i sumujemy zbiory *)

let rec podzielic a b = 
	match (a,b) with
	| (InterNan,_) -> InterNan
	| (_,InterNan) -> InterNan
	| (Inter1(ap,ak),Inter1(bp,bk)) ->
		if (bp = 0. && bk = 0.) then InterNan 
			else if (bp = 0.) then razy a (Inter1(1./.bk,inf)) 
				else if (bk = 0.) then razy a (Inter1(neg_inf,1./.bp)) 
					else if (bk *. bp < 0.) then	merge2  (razy a (Inter1(neg_inf, 1./.bp))) (razy a (Inter1(1./.bk, inf)))
						else razy a (Inter1(1./.bk,1./.bp))
	| (Inter2(ap1,ak1,ap2,ak2),Inter1(_,_)) ->
		let r1 = podzielic (Inter1(ap1,ak1)) b 
		and r2 = podzielic (Inter1(ap2,ak2)) b
			in merge2 r1 r2
	| (Inter1(_,_),Inter2(bp1,bk1,bp2,bk2)) ->
		let r1 = podzielic a (Inter1(bp1,bk1))
		and r2 = podzielic a (Inter1(bp2,bk2))
			in merge2 r1 r2
	| (Inter2(_,_,_,_),Inter2(bp1,bk1,bp2,bk2)) ->
		let r1 = podzielic a (Inter1(bp1,bk1)) 
		and r2 = podzielic a (Inter1(bp2,bk2))
			in merge2 r1 r2;;
(* dla dwoch Inter1 rozdzielamy przypadki, gdy dzielnik zawiera 0 *)
(* dla pozostalych rozbijamy na przedzialy i sumujemy zbiory wynikowowe *)

(*
let prz0 = Inter2 (neg_inf, 3., 5., inf);;
let prz1 = Inter1 (4.,5.);;
let prz2 = Inter1 (nan,nan);;
let prz3 = Inter1 (5.,10.);;
let prz4 = Inter1 (6.,inf);;
let prz5 = Inter1 (7.,11.);;
let prz6 = Inter1 (neg_inf, 3.);;
let prz7 = Inter2 (neg_inf, -6., 2., inf);;
let prz8 = Inter1 (-3.,-1.);;
let prz9 = Inter1 (-3.,2.);;
let prz10 = Inter1 (0.,2.);;
let prz11 = Inter1 (-1.,0.);;
let prz12 = Inter1 (0.,inf);;
let prz13 = Inter2 (neg_inf, 1., 2., inf);;
let prz14 = Inter2 (neg_inf, -.10., 10., inf);;
let prz15 = Inter2 (neg_inf, -.1., 1., inf);;
let prz16 = Inter1 (0.,0.);;
let prz17 = Inter1 (neg_inf, -6.);;
let prz18 = Inter1 (2., inf);;
let prz19 = Inter1 (1.,1.);;
*)