(* Autor : Jakub Kuklis *)
(* Code review : Marcin Mielniczuk *)

module A = Array
module L = List
module Q = Queue
module S = Set.Make(
	struct 
		type t = int array 
		let compare = Pervasives.compare 
	end)

let hash arr =
	let n = A.length arr
	and multip = 1009
	and modulo = 200003 in
	let rec pom acc k =
		if k = n then acc
		else pom ((multip * acc + arr.(k)) mod modulo) (k +1)
	in pom 0 0
(* zwraca pewna wartosc zalezna od stanu *)

let split arr =
	let n = A.length arr in
	let f = A.init n (fun i -> fst arr.(i))
	and s = A.init n (fun i -> snd arr.(i))
	in (n,f,s)
(* zwraca dlugosc i przerabia tablice par na pare tablic *)
			
let gcd a b =
	let rec eucl a b =
		if b = 0 then a else eucl b (a mod b)
	in eucl (max a b) (min a b)
(* nwd a b *)

let all_gcd arr =
	let n = A.length arr
	and res = ref 0 in
	for i = 0 to n-1 do
		res := gcd !res arr.(i)
	done;
	max !res 1
(* nwd wszystkich elementow listy *)

exception Success of int

let przelewanka arr =
	let (n,vol,final) = split arr
	and q = Q.create ()
	and h = Array.make 200009 S.empty
	(* w tablicy trzymamy sety - przyspiesza troche sprawdzanie czy stan *)
	(* juz sprawdzony, oraz zmniejsza szanse, ze kontener bedzie za duzy *)
	in if n = 0 then 0 else
	let pour st i j =
		let new_st = A.copy st 
		and to_pour = min (st.(i)) (vol.(j) - st.(j)) 
		in ( 
			new_st.(i) <- st.(i) - to_pour;
			new_st.(j) <- st.(j) + to_pour;
			new_st )
	(* przelanie z i do j *)
	and n_mem st = not (S.mem st h.(hash st))
	and ini = (A.make n 0, 0)
	and emp_or_full = 
		let b = ref false in (
		A.iteri (fun i x -> if x = 0 || x = vol.(i) then b := true) final;
		!b )
	(* czy ktoras pusta lub pelna *)
	and divides =
		let div = all_gcd vol 
		in A.fold_left (fun a h -> if h mod div != 0 then false else a) true final
	(* czy nwd wszystkich dzieli wszystkie oczekiwane objetosci *)
	in
	if not emp_or_full || not divides then -1 else
	try (
	Q.push ini q;
	while not (Q.is_empty q) do
		let (st,mov) = Q.pop q in
		if n_mem st then ( 
			if st = final then raise (Success mov);
			(* jezeli znajdziemy, to konczymy *)
			for i = 0 to n -2 do
				for j = i +1 to n -1 do
					let p_ij = pour st i j
					and p_ji = pour st j i in (
					if n_mem p_ij then Q.push (p_ij, mov+1) q;
					if n_mem p_ji then Q.push (p_ji, mov+1) q; )
				done
			done;
			(* wszystkie przelania *)
			for i = 0 to n -1 do
				let st_D = A.copy st 
				and st_F = A.copy st in
				( st_D.(i) <- 0;
				if n_mem st_D then Q.push (st_D, mov+1) q;
				st_F.(i) <- vol.(i);
				if n_mem st_F then Q.push (st_F, mov+1) q; )
			done;
			(* wszystkie wylania, dolania *)
			h.(hash st) <- S.add st h.(hash st); 
		)
	done; -1) with Success k -> k